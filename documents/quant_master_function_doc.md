# Quant Master 应该有的功能
1. 监控行情k线
2. 设置技术指标及各种相应信息
3. 监控账户持仓风险（包括紧急平仓等相关内容）
4. 显示运行中策略的log

# 2022-8-12设计初步实现内容
>&emsp;&emsp;基于tqsdk的web gui 功能，运行于本地（运行于服务器不好鉴权）
> 从Mysql获取Turtle的持仓，redis获取其他新增的（主要是创新高新低的标的）的代码
> 然后订阅这些标的，并画上SMA策略和turtle策略的技术指标图。
> 后续可以考虑加上策略的log

