cd /root/

cd ta-lib

./configure

make

make install

# 配置时间和talib的环境变量
echo "export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH" >> /etc/bash.bashrc
rm /etc/localtime
cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime


source /etc/bash.bashrc

pip install -i https://pypi.tuna.tsinghua.edu.cn/simple ta-lib

echo "40 8,20 * * 1-5 /bin/bash /root/runTrader.sh" | crontab

service cron start
# 死循环避免容器退出

while true
do
sleep 1
done

