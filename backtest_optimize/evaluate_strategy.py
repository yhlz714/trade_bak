"""
author: Yhlz
用来评估一个策略的适应度，在优化过程中通常不能像回测一样将所有的细节全都计算出来。
于是使用几个指标进行加权得出的结果算是一个分数，相对于简单的用卡玛比率或者夏普比率
这样相对来说更为科学。
"""
import numpy as np
import pandas as pd


def Yhlz_evaluate_zero(data:  np.ndarray):
    """
    初始的尝试，以前都是使用卡玛比率在优化的时候来评判这个参数是否优秀
    现在发现，卡玛比率最大回撤对于这个指标的影像太大。准备使用夏普，但
    是也会加上一些其他的权重，包含：
    0.4*sharpe(大） + 0.2*交易次数（相当于交易的成本）（小） + 0.3*平均创新高时间（是否可忍受）（小） + 0.1*获利品种个数（是否具有鲁棒性）（大）
    data应该是nd.array内部元组应是4列一行有多少参数此时就有多少行。
    :return:
    """
    # sharpe, trasnactionCount, avgNewHighTime, profitcount
    if isinstance(data, np.ndarray):
        if data.shape[1] == 4:
            pass
        else:
            raise Exception('应该传入4列n行，每行表示一个参数对应的4个数据！')
    else:
        raise Exception('传入参数应该是numpy矩阵！')
    data = pd.DataFrame(data,columns=['sharpe', 'avgCost', 'avgHighTime', 'profitCount'])
    data['sharpe'] = data['sharpe'].rank(ascending=False)
    data['profitCount'] = data['profitCount'].rank(ascending=False)
    data['avgCost'] = data['avgCost'].rank()
    data['avgHighTime'] = data['avgHighTime'].rank()

    # 返回值代表了传入的的顺序的适应度的排名，这个ndarray是越小越好的
    return (data['sharpe'] * 0.4 + data['avgCost'] * 0.2 + data['avgHighTime'] * 0.3 + data['profitCount'] * 0.1).rank()

if __name__ == 'main':
    test = np.random.randint(-10, 10, size=(5, 4))
    print(test)
    print(Yhlz_evaluate_zero(test))