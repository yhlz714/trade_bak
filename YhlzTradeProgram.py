# coding: gbk
# This is yhlz's trading program base on tqsdk.
# Main running progrem.

"""
Author: Yhlz 2020-03-06
"""
import threading
import os
import time
from datetime import datetime
import warnings
import platform
plt = platform.platform()
#print(f'test nohup.out -{time.ctime()}')
import logging
from logging.handlers import SMTPHandler
import traceback

import pandas as pd
import numpy as np
from tqsdk import TqApi, TqAuth, TqKq
import tushare as ts

import backtest_optimize.Strategy as stg
from RealModule import RealBroker, RealFeed, RealBars, ts2KQ, KQ2ts

# highlight 避免这种警告出现，真有问题报错再说：FutureWarning: elementwise comparison failed; returning scalar instead, but in the future will perform elementwise comparison
#   result = method(y)
warnings.simplefilter(action='ignore', category=FutureWarning)

class Context():
    """全局变量用于存储各种类型的东西"""
    Record_path = '/root/trade/config/Record.json'
    all_data_used_path = '/root/trade/config/all_data_used.txt'
    strtegy_to_run_path = '/root/trade/config/strategytorun.txt'
    current_account_path = '/root/trade/config/currentAccount.csv'
    general_ticker_info_path = '/root/trade/config/general_ticker_info.csv'
    error_count = {}

context = Context()

class Get_Input(threading.Thread):
    """this thread contral wheather to stop whole program"""

    def run(self):
        global contral
        while True:
            contral = input('press q to quit!')
            logger.debug(contral)
            logger.debug(time.ctime())
            now = time.localtime()
            if now.tm_hour == 15 or now.tm_hour == 3 or contral == 'q':
                logger.debug('Ready to quit')
                return


"""----------------------------------------------初始化阶段----------------------------------------------------------"""
logger = logging.getLogger('Yhlz')
# logger.setLevel(logging.DEBUG)
fm = logging.Formatter('%(asctime)s-%(created)f-%(name)s-%(levelname)s-%(message)s')

fh = logging.FileHandler('/root/trade/log/main.log')
fh.setFormatter(fm)
fh.setLevel(logging.DEBUG)

mail_handler = SMTPHandler(
        mailhost=('smtp.qq.com',587),
        fromaddr='517353631@qq.com',
        toaddrs='517353631@qq.com',
        subject='运行信息-dev',
        credentials=('517353631@qq.com', 'kyntpvjwuxfvbiag'))
mail_handler.setLevel(logging.WARNING)
mail_handler.setFormatter(fm)

logger.addHandler(mail_handler)
logger.setLevel(logging.DEBUG)
logger.addHandler(fh)
logger.propagate = False
mark = 0  # 控制不要一分钟多次发邮件

try: 
    if __name__ == '__main__':
        logger.debug('This is yhlz\'s trading server,now started!\n')

        # tushare 对象通过context对象传递给策略，否则不方便获取数据。
        tsObj = ts.pro_api('dddcca116f1989381f025f6bcc2cb8f3e5c6910ca222cfc2f286202e')
        context.tsObj = tsObj

        # 定义常量
        durationTransDict = {'1m': 60, '1d': 86400}

        try:
            api = TqApi(TqKq(), auth=TqAuth("18064114200", "86888196"))
            # api = TqApi(TqAccount('simnow', '133492', 'Yhlz0000'), web_gui=False)
            logger.critical('success sign in! with Q7')
            # api = TqApi(TqAccount('H华安期货', '100909186', 'Yhlz0000'), auth=TqAuth("18064114200", "86888196"))
            # logger.info('success sign in! with 100909186')

        except Exception as e:
            logger.error('problem with sign in!')
            exit(1)

        f = open(context.strtegy_to_run_path)
        temp = f.readlines()
        f.close()

        '---------------------------------------------------初始化数据------------------------------------------------------'

        strategys = {}
        allTick = {}
        allKline = RealFeed()
        # 初始化策略
        pos = api.get_position()
        orders = api.get_order()

        # realAccount = pd.read_csv(self.context.current_account_path)

        contractInfo = pd.concat(
             [tsObj.fut_basic(exchange='SHFE'), tsObj.fut_basic(exchange='DCE'),
              tsObj.fut_basic(exchange='CZCE'), tsObj.fut_basic(exchange='CFFEX'),
              tsObj.fut_basic(exchange='INE')])
        #contractInfo = tsObj.fut_basic(exchange='SHFE')  #test  只要上期所主力，避免时间太长
        #contractInfo = contractInfo.iloc[:100, :]  # test 只取前十个
        # contractInfo = tsObj.fut_basic(exchange='SHFE')  # test 暂时在测试只用上期所，避免郑商所花费太长时间。
        # 只获取当前还没有退市的合约。
        now = datetime.now()
        contractInfo = contractInfo[contractInfo['delist_date'] > datetime.now().strftime('%Y%m%d')]
        contractInfo=contractInfo.loc[~ contractInfo['symbol'].str.contains('TAS')]  
        # tushare原油包含一个 sctas的奇怪合约，将其去掉
        context.contractInfo = contractInfo

        allStgBar = {}
        allStgTick = {}
        broker = RealBroker(api, context, position=pos)
        # 设置所有策略的基策略的交易模式。
        stg.YhlzStreategy.realTrade = True
        stg.YhlzStreategy.realBroker = broker
        # 给策略传递参数，后面两个必须参数先传空字符， 默认参数不传，因为策略不会重名，所以每个策略的默认参数就是运行参数。

        # 初始化所有需要的数据
        for item in temp:
            logger.debug(f'读取到的strategytorun是{temp}')
            item = eval(item)
            # !!! 策略不可重名， 如需同策略不同参数，可以继承一个，然后换个名字。
            strategys[item[0]] = item[1:]  # 将strategy to  run 中的策略对应的 名字和合约记录下来。
            for dataNeeded in item[1:]:
                if dataNeeded[1] == 'tick':  # 海龟交易法需要tick触发
                    if dataNeeded[0] == '*':  # 海龟交易时订阅所有的tick。
                        logger.debug(f'出现*订阅所有tick')
                        for symbol in contractInfo['ts_code']:
                            logger.debug(f'{symbol}')
                            symbol = ts2KQ(symbol)
                            temp_instrument.append(symbol)
                            if symbol not in allTick:
                                logger.debug(f'订阅tick：{symbol}')
                                try:
                                    allTick[symbol] = api.get_quote(symbol)
                                    # 如果是指数合约那么把对应的主力合约也订阅上。
                                    if 'KQ.i' in symbol and symbol.replace('KQ.i', 'KQ.m') not in allTick:
                                        logger.debug(f'订阅指数的主力合约：{symbol}, 频率：{dataNeeded[1]}')
                                        allTick[symbol.replace('KQ.i', 'KQ.m')] = \
                                            api.get_quote(symbol.replace('KQ.i', 'KQ.m'))
                                        allTick[allTick[symbol.replace('KQ.i', 'KQ.m')].underlying_symbol] = \
                                           api.get_quote(allTick[symbol.replace('KQ.i', 'KQ.m')].underlying_symbol)
                                except Exception as e:
                                    logger.info(f'{symbol} 这个合约订阅tick出现问题！{e}')
                        continue  # 订阅了带*的tick后就不需要再订阅了因为已经订阅了所有的了
                    # TODO 还需要补充不是‘*’的情况，目前只有海龟，所以先不写，在此缩进。
                else:
                    if dataNeeded[0] == '*':  # 星号表示所有这个周期的合约都要订阅这个长度
                        logger.debug(f'出现*订阅所有行情')
                        temp_instrument = []
                        for symbol in contractInfo['ts_code']:
                            logger.debug(f'{symbol}')
                            symbol = ts2KQ(symbol)
                            temp_instrument.append(symbol)
                            if symbol+dataNeeded[1] not in allKline:  # symbol + dataneeded[1]标的加频率共同构成了唯一的键
                                logger.debug(f'订阅k线：{symbol}频率：{dataNeeded[1]}')
                                try:
                                    allKline.addDataSource(symbol+dataNeeded[1],
                                                       api.get_kline_serial(symbol, durationTransDict[dataNeeded[1]],
                                                                            dataNeeded[2]))
                                except Exception as e:
                                    logger.info(f'{symbol} 这个合约订阅k线出现问题！{e}')
                                # allKline.addDataSource(symbol+dataNeeded[1],
                                #                        api.get_kline_serial(symbol, durationTransDict[dataNeeded[1]], dataNeeded[2]))
                            # if symbol not in allTick:
                            #     logger.debug(f'订阅tick：{symbol}频率：{dataNeeded[1]}')
                            #     try:
                            #         allTick[symbol] = api.get_quote(symbol)
                            #         # 如果是指数合约那么把对应的主力合约也订阅上。
                            #         if 'KQ.i' in symbol and symbol.replace('KQ.i', 'KQ.m')+dataNeeded[1] not in allTick:
                            #             logger.debug(f'订阅指数的主力合约：{symbol}, 频率：{dataNeeded[1]}')
                            #             allTick[symbol.replace('KQ.i', 'KQ.m')] = \
                            #                 api.get_quote(symbol.replace('KQ.i', 'KQ.m'))
                            #             allTick[allTick[symbol.replace('KQ.i', 'KQ.m')].underlying_symbol] = \
                            #                 allTick[symbol.replace('KQ.i', 'KQ.m')]
                            #     except :
                            #         logger.error(f'{symbol} 这个合约订阅tick出现问题！')
                        item[1][0] = temp_instrument  # 避免传递给broker的strategys中出现*，在这里替换成具体的所有合约
                        continue
                    if dataNeeded[0]+dataNeeded[1] not in allKline:
                        logger.debug(f'订阅k线：{dataNeeded[0]}频率：{dataNeeded[1]}')
                        allKline.addDataSource(dataNeeded[0]+dataNeeded[1],
                                               api.get_kline_serial(dataNeeded[0], durationTransDict[dataNeeded[1]],
                                                                    dataNeeded[2]))
                    if dataNeeded[0]+dataNeeded[1] not in allTick:
                        logger.debug(f'订阅tick：{dataNeeded[0]}频率：{dataNeeded[1]}')
                        allTick[dataNeeded[0]] = api.get_quote(dataNeeded[0])
                        # 如果是指数合约那么把对应的主力合约也订阅上。
                        if 'KQ.i' in dataNeeded[0] and dataNeeded[0].replace('KQ.i', 'KQ.m')+dataNeeded[1] not in allTick:
                            logger.debug(f'订阅指数的主力合约：{dataNeeded[0]}, 频率：{dataNeeded[1]}')
                            allTick[dataNeeded[0].replace('KQ.i', 'KQ.m')] = \
                                api.get_quote(dataNeeded[0].replace('KQ.i', 'KQ.m'))
                            allTick[allTick[dataNeeded[0].replace('KQ.i', 'KQ.m')].underlying_symbol] = \
                                api.get_quote(allTick[dataNeeded[0].replace('KQ.i', 'KQ.m')].underlying_symbol)
        context.strategys = strategys
        context.allTick = allTick
        # 所有的需要的数据获取到以后再来初始化策略
        for item in temp:
            item = eval(item)
            for dataNeeded in item[1:]:
                if dataNeeded[1] == 'tick':
                    if str(item) not in allStgTick:
                        if str(item) in allStgBar:
                            allStgTick[str(item)] = allStgBar[str(item)]   # 如果tick那边初始化过了就不用再在这边初始化了
                        else:
                            allStgTick[str(item)] = eval('stg.' + item[0] + '(allKline, dataNeeded[0], context, {})')
                            # TODO dataNeeded[0]只是一个标的，还需要将所有标的记下来然后一起传给策略。
                        broker.strategyNow = allStgTick[str(item)]
                else:
                    if str(item) not in allStgBar:
                        if str(item) in allStgTick:
                            allStgBar[str(item)] = allStgTick[str(item)]  # 如果tick那边初始化过了就不用再在这边初始化了
                        else:
                            # bar需要用标的和频率来做键值，否则可能会将不同频率的同标的覆盖前面的
                            allStgBar[str(item)] = eval('stg.' + item[0] + '(allKline, dataNeeded[0], context, {})')
                        broker.strategyNow = allStgBar[str(item)]

        # TODO 订阅所有的主力合约的quote传给broker setALLMainContract
        generalTickerInfo = pd.read_csv(context.general_ticker_info_path)
        allMainContract = {}
        for symbol in generalTickerInfo['contract_name']:
            allMainContract[symbol] = api.get_quote(symbol)
        broker.setAllMainTick(allMainContract)
        # 因为不能先传递strategy列表进入（初始化strategy需要broker）所以初始化strategy后再初始化broker需要strategy的内容
        broker.setAllTick(allTick)
        broker.setStrategys(strategys)
        broker.creatAccountHelp()
        logger.debug(f'记录此时的k线:\n{allKline}\n记录此时的tick：\n{allTick}')

        """----------------------------------------------开始实盘运行阶段-------------------------------------------- """
        # print('初始化完成')
        contral = ''  # use for contral the whole program stop or not ,if it equal to 'q' then progam stoped
        # get_input=Get_Input()
        # get_input.setDaemon(True)
        # get_input.start()
        now_record = 61  # record minute time
        when = 0
        bars = RealBars()
        logger.debug('开始实时运行')
        second = 0
        upList = []
        lowList = []
        upperLimitList = []
        lowerLimitList = []

        # TODO 现在策略少，不需要太多的管理，如果以后策略多了，需要实现按照每个策略订阅的行情来推送。
        runTick = 0
        interval = 5  # 决定了多久检查一次onTick。每个tick都处理，python效率过低。无法完成
        tail_msg = ''  # 用来记录tail命令获取到的内容，判断，避免重复报错
        while True:
            api.wait_update(time.time() + 1)
            now = time.localtime()
            if now.tm_min == 30 or now.tm_min == 0:
                time.sleep(2)  # avoid some exception like when updated and send order but exchange refused ,then wait 2s

            if now.tm_min == 59:  # avoid 59 tick ,but can't trade, lead to some problem
                time.sleep(1)
                continue
            if runTick < time.time():
                runTick = time.time() + interval
                for strategy in allStgTick:
                    ticks = []
                    for tick in allTick:
                        if 'KQ' in allTick[tick].instrument_id:  # 指数和主力合约直接跳过不判断。
                            continue
                        if api.is_changing(allTick[tick]):
                            broker.strategyNow = allStgTick[strategy]
                            ticks.append(allTick[tick])
                    if ticks:
                        logger.debug(f'tick长度为{len(ticks)}')
                        allStgTick[strategy].onTickTest(ticks)
                        logger.debug(f'tick运行完成')

            run = False
            for contract in allKline.keys():  # 如果有变化了的就去运行。
                if api.is_changing(allKline[contract].data.iloc[-1], 'datetime'):  # have a new bar
                    run = True
                    break
            # logger.debug('判断run')
            if run:
                logger.debug('running!')
                # time.sleep(0.5)
                run = 0

                # 准备这个周期的bars
                tempDict = {}
                for contract in allKline:
                    tempDict[contract] = allKline[contract].data.iloc[-1, :]  # 最新一个bar
                bars.setValue(tempDict)
                for strategy in allStgBar:
                    broker.strategyNow = allStgBar[strategy]
                    allStgBar[strategy].onBars(bars)
                if not now.tm_min % 15:  # 当时间分钟数整除15时，也就是十五分钟运行一次,
                    logger.info('正常运行！')

            # 一秒钟更新一次

            if second != now.tm_sec:
                second = now.tm_sec
                # 每隔一秒进行一次检查。
                all_filled_order = broker.update()  # 返回dict of list
                logger.debug(f'filled order is :{all_filled_order}')
                for stg in all_filled_order:
                    if all_filled_order[stg]:  # 如果该策略有完成的订单，调用 on_order_finished
                        logger.debug(f'{stg} has filled order!')
                        for Stg in allStgBar:
                            if type(Stg).__name__ == stg:
                                logger.debug('call on order finished!')
                                for order in all_filled_order[stg]:
                                    Stg.on_order_finished(order)
                        # TODO 现在只处理bar的，tick的后面有需要再说。

                if 'Linux' in plt:
                    temp = os.popen('tail ./log/output.log -n 1').read()
                    if tail_msg != temp:  # 避免没有新行重复发邮件，记录内容，直到有新的警告信息在发邮件
                        if not ('DEBUG' in tail_msg or 'INFO' in tail_msg):
                            tail_msg = temp
                            logger.warning(tail_msg)
                        # 直接将报错的内容记录log，并且发邮件警示

            # 检测是否接近涨跌停预警
            for item in allTick:
                if not allTick[item].instrument_id in upList :
                    if (allTick[item].upper_limit - allTick[item].last_price) / (allTick[item].upper_limit - allTick[item].pre_settlement) < 0.1:
                        logger.warning(allTick[item].instrument_id + ' 接近涨停')
                        upList.append(allTick[item].instrument_id)
                        if allTick[item].upper_limit == allTick[item].last_price:
                            upperLimitList.append(allTick[item].instrument_id)

                elif not allTick[item].instrument_id in lowList:
                    if (allTick[item].last_price - allTick[item].lower_limit ) / (allTick[item].pre_settlement - allTick[item].lower_limit) < 0.1:
                        logger.warning(allTick[item].instrument_id + ' 接近跌停')
                        lowList.append(allTick[item].instrument_id)
                        if allTick[item].lower_limit == allTick[item].last_price:
                            lowerLimitList.append(allTick[item].instrument_id)

                elif allTick[item].instrument_id in upList:
                    if (allTick[item].upper_limit - allTick[item].last_price) / (allTick[item].upper_limit - allTick[item].pre_settlement) > 0.1:
                        logger.warning(allTick[item].instrument_id + ' 离开涨停附近')
                        upList.remove(allTick[item].instrument_id)
                        if allTick[item].instrument_id in upperLimitList and allTick[item].upper_limit != allTick[item].last_price:
                            upperLimitList.remove(allTick[item].instrument_id)

                elif allTick[item].instrument_id in lowList:
                    if (allTick[item].last_price - allTick[item].lower_limit) / (allTick[item].pre_settlement - allTick[item].lower_limit) > 0.1:
                        logger.warning(allTick[item].instrument_id + ' 离开跌停附近')
                        lowList.remove(allTick[item].instrument_id)
                        if allTick[item].instrument_id in lowerLimitList and allTick[item].upper_limit != allTick[item].last_price:
                            lowerLimitList.remove(allTick[item].instrument_id)

                # 检查涨跌停合约的持仓
                for position in pos:
                    if pos[position].instrument_id in lowerLimitList and pos[position].pos_long > 0:
                        logger.warning(pos[position].instrument_id + ' 有在跌停的多仓！')
                    elif pos[position].instrument_id in upperLimitList and pos[position].pos_short > 0:
                        logger.warning(pos[position].instrument_id + ' 有在涨停的空仓！')

                # 检查涨跌停合约的挂单。
                for order in orders:
                    if not orders[order].is_dead:
                        if orders[order].instrument_id in lowerLimitList:
                            logger.warning(orders[order].instrument_id + ' 有在跌停的挂单！')
                        elif orders[order].instrument_id in upperLimitList:
                            logger.warning(orders[order].instrument_id + ' 有在涨停的挂单！')

            if now.tm_hour == 3 or (now.tm_hour == 15 and now.tm_min == 15)or contral == 'q':
                break
            # logger.debug('循环一周')

except Exception as e:  
    logger.error('运行出现问题请立即检查！\n' + traceback.format_exc())

finally:  
    for stg in allStgBar:
        allStgBar[stg].stop()
    
    for stg in allStgTick:
        allStgTick[stg].stop()

    if 'broker' in dir():
        broker.stop()
    api.close()

    logger.critical('stop running!')
    fh.close()  # 关闭logg文件
    # 压缩log改名备份
    os.system(f"tar -zcf ./log/{time.ctime().replace(' ', '_').replace(':', '_')+ 'main_log.tar.gz'} ./log/main.log")
    os.system('rm -f ./log/main.log')