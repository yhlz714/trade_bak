import time

import ccxt
from ccxt.base.errors import OrderNotFound
import threading
import random
import logging
import functools
# from Function import *

# 此处使用的是测试的apikey和secret
BINANCE_CONFIG = {
    'apiKey': 'e20828531d57bd2a77280283bd719ccd6fa1428f3f77f20e2685c071bbdf7260',
    'secret': 'e9da80281c31e14cc655c32fc996d24d6881eda9df85903acab79901c6c96c8d',
    'timeout': 3000,
    'rateLimit': 10,
    'verbose': False,
    'hostname': 'https://testnet.binancefuture.com',
    'enableRateLimit': False,
    'proxies': {'https': "http://127.0.0.1:7890", 'http': "http://127.0.0.1:7890"}
}

EXCHANGE = ccxt.binance(BINANCE_CONFIG)
EXCHANGE.set_sandbox_mode(True)

symbol_config = {
    # 'ETHUSDT': {'leverage': 1,
    #                  'strategy_name': 'real_signal_simple_bolling',
    #                  'para': [660, 1],
    #                  'position': 1,
    #                  },
    'ETHUSDT': {'leverage': 1,
                'strategy_name': 'real_signal_random',
                'para': [660, 1],
                'position': 0.1,
                },
}

# =获取交易精度
# usdt_future_exchange_info(EXCHANGE, symbol_config)


if __name__ == '__main__':
    od = {'symbol': 'ETHUSDT',
          'side': 'SELL',
          'price': '1150.6',
          'type': 'LIMIT',
          # 'timeInForce': 'GTX',  # 只做maker
          'timeInForce': 'GTC',
          'quantity': 10}
    param = {
        "timestamp": int(time.time() * 1000)
    }
    r = EXCHANGE.fapiPublicGetPing()
    print(r)
    # res = EXCHANGE.fapiPrivatePostOrder(params=od)
    # p = {"symbol": 'ETHUSDT', 'orderId': res['orderId'],
    #                          'timestamp': int(time.time() * 1000)}
    # res = EXCHANGE.fapiPrivate_get_income(param)
    # time.sleep(2)
    # try:
    #     EXCHANGE.fapiPrivateDeleteOrder(params=p)
    # except OrderNotFound:
    #     print('get!')
    # print(res)